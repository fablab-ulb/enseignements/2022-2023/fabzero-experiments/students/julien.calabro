## Avant propos

Bonjour et bienvenue sur mon site pour le cours PHYS-F517. C'est ici que je documente mon travail sur les différents modules du cours. Je l'ai choisis pour développer de nouvelles compétences telles que l'impression 3D, la découpe laser et autre. A la fin du cours, on réalise un projet en groupe dont la documentation se trouve ailleurs.

J'ai été assez surpris par la quantité de travail que cela demande, je pensais qu'on allait se concentrer sur les formations et le projet et un peu moins sur la documentation et les outils de gestion de projet. Mais bon, toutes les bonnes choses ont un mauvais côté, je dois m'y plier pour que ma formation soit complète.


## À propos de moi

![](images/MWA.jpg)

Saludo ! Moi c'est Julien Calabro dit Calabrax. J'ai 22 ans, suis en Master 2 ingénieur physicien en radiophysique médicale. J'aime les pâtes, le limoncello et l'escalade. Il y a 2 domaines là dedans dans lesquels j'excelle.

## Mon background

Comme dit précédemment, je suis étudiant à l'école polytechnique de Bruxelles, en filière physique. J'aime beaucoup ces études parce qu'elle me permettent de mieux comprendre le monde qui m'entoure, aussi bien la nature que les inventions humaines. Je me suis orienté en radiophysique médicale parce que c'est l'option où je peux avoir un réel impact sur la vie des gens. Me sentir utile est vraiment quelque chose d'imortant pour moi.

En plus des études, je fais beaucoup de choses, cette année j'ai organisé un festival pour le Cercle Polytechnique, l'année prochaine j'organise la JobFair engineers pour aider les étudiant.e.s en polytech à trouver un boulot. Je suis également élu au BEA (Bureau  des Etudiant.e.s dministrateur.trices.s) avec lequel je siège dans plusieurs commisons de l'ULB. Je représente notamment les étudiant au Conseil d'Administration de l'université, à la Commission Culturelle, à la Commission aux Affaires Sociales Etudiantes et d'autres encore.

Je vais pas dire tout ce que je fais parce que la liste est encore longue mais voilà mes principales activités. Tout ça me demande du temps pendant l'année ce qui fait que je délaisse un peu mes cours durant le quadri. Je les ouvres uniquement en blocus/session mais ça ne me pose pas trop de problèmes. Par contre, c'est parfois plus compliqué pour les projets et les travaux en continu. Ceci explique le retard que j'ai de temps en temps.

