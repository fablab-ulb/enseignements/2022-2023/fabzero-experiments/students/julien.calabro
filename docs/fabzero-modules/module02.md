# 2. Conception Assistée par Ordinateur (CAO)

Cette section reprend les différentes étapes pour réaliser de la conception assistée par ordinateur. En gros, comment créer un objet en 3D avec un ordinateur dans le but de l'imprimer en 3D.

## 2.2 Logiciels de conception

J'ai utilisé 3 logiciel de conception gratuit différents, voici leur nom et le lien pour les télécharger:

* [Inkscape](https://inkscape.org/fr/release/inkscape-0.92.4/)
* [OpenSCAD](https://openscad.org/)
* [freeCAD](https://www.freecad.org/downloads.php?lang=fr)

### 2.2.1 Inkscape

Inkscape est un logiciel de conception pour réaliser des images en 2D. Il est possible à partir de celles-ci de créer un objet 3D en faisant une extrusion linéaire ou une rotation. Je ne l'ai pas vraiment utilisé de cette façon donc je n'expliquerais pas ici comment faire. Il ne m'a donc pas été très utile pour la conceptio 3D. En revanche, pour faire une découpe laser, il peu s'avérer très puissant. Son fonctionnement est repris dans la section correspondant à la découpeuse laser.

### 2.2.2 OpenSCAD

Ce logiciel permet de réaliser des formes simples tel que des parallélépidèdes rectangle, des sphères, des cylindres ou encore des polyèdres. En assemblant ces solide, il est possible de construire des géométries plus complexe mais certaines formes sont impossible à faire. Pour donner un exemple, le logo de twitter est un simple asseblage de cercle et d'arc de cercles. Il est donc possible de le dessiner avec OpenSCAD. Par contre le ruban de moebius est impossible à faire.

![Logo twitter](images/twitter.jpg)
![Ruban de Moebius](images/moebius.jpg)

Pour concevoir un objet, différentes commande pour créer des formes, faire des rotations, translations, extraction de matières etc. Les fichiers .scad sont simplement des fichiers textes contenant le code qui génère les figures. Ils prennent donc très peu de place et sont facilement modifiables. Ce logiciel à l'avantage d'être facile d'utilisation si on est à l'aise pour coder, ce qui est mon cas.

### Commandes de base

pour écrire en openSCAD il ne faut pas oublier de ponctuer chaque fin de ligne avec un point virgue ";".

Voici quelques commandes de base :

Cube :

` cube(size) `

Paraléllépipède rectangle : 

`cube([largeur, profondeur, hauteur]); `

Sphère : 

` sphere(rayon) `

Cylindre : 

`cylinder(h = hauteur, r = rayon)`

Cône : 

`cylinder(hauteur, rayon du bas, rayon du haut)`

Polyèdre réguliers : 

`cylinder(h=hauteur, r = rayon, $fn = nombre de faces latérales)`

Il est possible de faire quelque chose de similaire avec la fonction `sphere()`.

Translation :

`translate([x, y, z])`

Rotation : 

`rotate([x, y, z])`

Faire la différence entre 2 formes :

```
difference(){
  forme 2

  forme 2
}
```
Faire un "mélange" entre 2 formes : 

```
minkovski(){
  forme 1

  forme 2
}
```

Créer une fonction : 

```
module nom_fonction(paramètre 1, paramètre 2, ...) {

  contenu de la fonction

}
```

Un code implémentant ces fontion est repris plus bas dans la section 2.3 dans laquelle je réalise un flexlink sur OpenSCAD dans le but de l'imprimer en 3D.

### 2.2.3 FreeCAD

## 2.3 Flexlink

Le but de cette section est de desinger  un flexlink en 3D sur OpenSCAD sans le ut de l'imprimer par la suite. Le processus d'impression est décrit dans le module 3, je n'en parlerais pas ici.

Un flexlink est une pièce en plastique capable de produire un ouvement n se déformant. C'est un matériau flexible. Je veut donc créer 2 blocs rectangulaire relier entre eux par un arc de cercle pliable. Je veux aussi avoir un système pour assembeler ma pièce avec une autre. Pour ce faire, je crée des trous dans les blocs à intervalle réguliers.


### 2.3.1 Les blocs

Je commence par crer un bloc que je translate.

``` 
height = 15;
depth = 50;
width = 10;
distance = 50;

translate([0, distance/2, 0])
cube([width, depth, height]);
```

Le paramètre "distance" correspond à la distance qu'il y aurait si les 2 blocs étaient face à face. J'obtiens le résultat suivant:

![](images/cube.png)

Je peux maintenant créer le 2e bloc que je translate et tourne de 90 degrés. Je rajoute les lignes de codes suivantes:

``` 
rotate([0, 0, 90]){        
translate([0, -depth -distance/2, 0])
cube([width, depth, height]);
}
``` 
Ce qui donne:

 ![](images/2cubes.png)

### 2.3.2 Les trous

Je veux que les trous soient circulaire et traversent le blocs de part et d'autre. J'utilise alors les fonctions `cylinder()` et `difference()`. J'inclu le tout dans une boucle `for`. Le code devient alors :

``` 
height = 15;
depth = 50;
width = 10;
radius = 2;
distance = 50;
$fn = 50;


    difference(){
translate([0, distance/2, 0])
cube([width, depth, height]);

        
for ( i = [distance/2 + radius + d: radius + d : distance/2 + depth - d] ){ 
translate([0, i, height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
    
translate([0, i, 2*height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
}
}

rotate([0, 0, 90]){
    difference(){            
translate([0, -depth -distance/2, 0])
cube([width, depth, height]);

        
for ( i = [-distance/2 - depth  + radius + d: radius + d : -distance/2 - d] ){ 
    
translate([0, i, height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
    
translate([0, i, 2*height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
}
}
}
```

Ce qui donne :

![](images/cubeettrous.png)


### 2.3.3 L'arc de cercle

Il ne reste plus qu'a faire l'arc de cercle qui relie les 2 blocs. Ici l'idée est de créer 2 disques de rayon différents avec la fonction `circle()`. Ensuite, on fait la différence entre les 2 pour qu'il ne reste qu'un anneau. On construit ensuite un triangle qu'on va soustraire à l'anneau pour avoir un arc de cercle avec l'ouverture angulaire souhaitée. Pour terminer je réalise une extrusion linéaire pour avoir une pièce en 3D avec la hauteur souhaitée.

Voici le code final :

``` 
height = 15;
minih = 3;
depth = 50;
width = 10;
radius = 2;
thickness = 1;
d = 3;
distance = 50;
r1 = distance/2 - width/2 + thickness/2;
r2 = distance/2 - width/2 - thickness/2;
a1 = -5;
a2 = 95;
hcentering = height/2 - height/10;
wcentering = width/2 - width/40;
$fn = 200;


    difference(){
translate([0, distance/2, 0])
cube([width, depth, height]);

        
for ( i = [distance/2 + radius + d: radius + d : distance/2 + depth - d] ){ 
translate([0, i, height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
    
translate([0, i, 2*height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
}
}

rotate([0, 0, 90]){
    difference(){            
translate([0, -depth -distance/2, 0])
cube([width, depth, height]);

        
for ( i = [-distance/2 - depth  + radius + d: radius + d : -distance/2 - d] ){ 
    
translate([0, i, height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
    
translate([0, i, 2*height/3])
rotate([0, 90, 0])
    cylinder(h = width, r = radius);
}
}
}

module arc(r1, r2, a1, a2) {
  difference() {
    difference() {
      polygon([[0,0], [cos(a1) * (r1 + 50), sin(a1) * (r1 + 50)], [cos(a2) * (r1 + 50), sin(a2) * (r1 + 50)]]);
      circle(r = r2);
    }
    difference() {
      circle(r=r1 + 100);
      circle(r=r1);
    }
  }
}


rotate ([0, 0, 180])
translate([-distance/2, -distance/2, hcentering - minih/2])
linear_extrude(height = minih){
arc(r1, r2, a1, a2);
}
```

![](images/flexlink.png)

## 2.3 Licenses Creatives Commons (CC)

Les Licenses CC permettent de partager son travail tout en le protégeant au niveau de la loi. Grâce à elles, je peux utiliser le travail de quelqu'un d'autre en respectant certaines conditions fixées par son auteur.

Il exsite 4 abréviations pour caractériser une license:

| Abréviation| Signification|
| --------------------|---------------------|
|BY ![BY](images/BY.png)| Il faut créditer le créateur|
|NC ![NC](images/NC.png)| Le travail ne peut pas être utilisé à des fins comerciales|
|ND ![ND](images/ND.png)| Aucune modification ni adaptation n'est permise|
|SA ![SA](images/SA.png)|Les adaptation sont autorisées mais partager selon les mêmes conditions|

Il est également possible d'utiliser la license CC0 pour que notre travail soit du domaine public, c'est à dire que le travail est partagé sans conditions. En combinant ces abréviations, il est possible de créer 6 types de licenses : 

- CC BY
- CC BY-SA
- CC BY-NC
- CC BY-NC-SA
- CC BY-ND
- CC BY-NC-ND

Il faut aussi mentioner le lien de la license. Plus d'infos sur les licenses CC sont disponible [ici](https://creativecommons.org/about/cclicenses/).