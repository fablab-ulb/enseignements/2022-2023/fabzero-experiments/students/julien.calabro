# 4. Outil Fab Lab sélectionné : Laser Cutter

Cette semaine, j'ai suivis la formation permettant d'utiliser une découpeuse laser. J'ai utilisé le logiciel de conception inkscape. Je vais donc décrire ici comment j'ai réalisé un shuriken. Je décris aussi brièvement chaque machine.

## 4.1 Quelques règles

Utiliser de telles machine nécessite quelques précaution. En effet certains matériau ne sont pas compatible et d'autres règles de sécurité sont en vigueur. Elles sont toutes reprise dans [ce guide](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md).

## 4.2 Descrition des machines

### 4.2.1 Epilog Fusion Pro 32


- Surface de découpe : 81 x 50 cm
- Hauteur maximum : 31 cm
- Puissance du LASER : 60 W
- Type de LASER : CO2 (infrarouge)
- Logiciel : Epilog Dashboard

C'est la plus facile à utiliser, il suffit de mettre le fichier .svg sur une clef USB, règler la puissance et la vitesse puis lancer la découpe. Elle dispose d'une caméra permetant d'être sûr que l'objet à découper/graver ce trouve bien sur la plaque de bois ou de plexi. Plus d'information ce trouve dans son [manuel d'utilisation](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md).

### 4.2.2 Lasersaur


- Surface de découpe : 120 x 60 cm
- Hauteur maximum : 12 cm
- Vitesse maximum : 4000 mm/min
- Puissance du LASER : 100 W
- Type de LASER : CO2 (infrarouge)
- Logiciel : Driveboard App

Celle ci est plus compliquée à utiliser à cause de son logiciel.Plus d'information ce trouve dans son [manuel d'utilisation](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md).

### 4.2.3 Full Spectrum Muse

- Surface de découpe : 50 x 30 cm
- Hauteur maximum : 6 cm
- Puissance du LASER : 40 W
- Type de LASER : CO2 (infrarouge) + pointeur rouge
- Logiciel : Retina Engrave

L'avantage de celle-ci c'est qu'on peut envoyer des fichier à distance avec le wifi. On doit juse utiliser le logiciel sur navigateur "Retina Engrave" et se connecter au wifi de la machine avec le mdp fablabULB2019. Plus d'information sont disponibles en suivant les [tutoriels vidéos](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6).

## 4.3 Réalisation d'un shuriken

### 4.3.1 Calibration grid

Avant d'utilisr une machinee pour la première fois avec un nouveau matériau, il peut être utile de découper une "calibration grid". Celle ci permet de voir quels sont les effets des différent paramètres (vitesse et puissance). Voici à quoi ressemble une tele grille :

![calibration](images/calibration_grid.jpg)

### 4.3.2 Shuriken

Pour se familiaisé avec la machine (l'épilog) j'ai utilisé une image de shuriken en format svg trouvée sur internet sur lequel j'ai rajouté une petite étoile à graver ainsi que les inscripions "J&G" (pour Julien et Gilles avec qui j'ai suivis la formation). Le résultat est le suivant :

![shuriken](images/shuriken.jpg)

