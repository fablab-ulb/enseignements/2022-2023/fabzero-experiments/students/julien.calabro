# 5. Dynamique de groupe et projet final

Dans cette section, nous allons voir quelques outils utile à la réalisation d'un projet de groupe. Tout d'abord, nous allos voir ce qu'est un abre à problème et à objectif (problem tree and objectif tree en anglais). Je l'appliquerai à un problème particulier. Ensuite, j'explique comment les groupes de projet ce sont formés et quels outils de gestion de projets nous on été donnés.

## 5.1 Problem tree and Objective tree

Le problème sur lequel j'ai choisi de travailler est la perte d'objets. Par exemple, ça nous est déjà toutes et tous arriver de ne plus trouver nos clefs au moment où on doit partir et qu'on est pressé. Cela peut vite devenir très embêtant. C'est sur cette base que je construit mon problem tree.

Cet arbre est composé de 3 parties :

- Le tronc : le problème en lui-même
- Les racines : les causes du problèmes
- les branches : les conséquences du problème

Où les différentes ramifications sont les conséquences des conséquences ou les causes des causes.

Voici ce que cela donne :

![](images/problem_tree.png)

Une fois cet arbre fait, je peux, à partir de celui-ci, réaliser un objective tree. Les différentes parties sont :

- Le tronc : Je reformule le problème en objectif
- Les racines : Les solutions à mettre en place pour empêcher le problème
- Les branches : Les résultats de ces solutions

Voici ce que j'obtiens : 

![](images/objective_tree.png)

## 5.2 Formation des groupes

Pour former les groupes de projet et éviter se se mettre qu'avec ses amis, nous devions tous apporter un objet en classe qui symbolisait, pour nous, une problématique qui nous tennait à coeur. Ensuite, on s'est baladé dans la classe pour trouver plusieurs personnes qui ont un objet/problématique similaire.

### 5.2.1 Mon objet

![](images/D.jpg)

Vous l'aurez compris, j'ai décidé de ramener un dé à 6 faces. Il symbolise l'addiction aux jeux d'argent et les addictions en général (la drogue, l'alcool (qui est aussi une drogue au passage), la cigarette, le sucre, etc). Ce genre d'addiction est très néfaste et touche plus de monde qu'on ne le croit, c'est donc une problématique qui me tenais à coeur.

### 5.2.2 Mon groupe

Trouver quelqu'un avec un objet ressemblant au mien d'a pas été chose aisée. Je n'ai d'ailleurs pas réussi. Je me suis donc retrouvé dans un groupe assez hétéroclyte et la seule chose qu'on avait en commun c'était qu'on avait trouvé personne. Nous étions peut-être un peu trop originaux. Les membres de mon groupe et leur objets respectifs sont : 

- Serge : un bol pour la famine
- Morgan : un écocup pour l'acologie et la consomation de plastique
- Léon : un coeur de pigeon pour la santé

## 5.3 Brainstorming

Une fois le groupe formé, nous avons cherché un thème qui nous rassemblait tous les 4. On s'est mis autour d'une feuille, on a écrit notre objet sur les 4 cotés de la feuille puis on a commencé à faire des liens entre nos différents objets et problématiques. Nous avons noté tout ce qui nous passait par la tête, même les idée les plus loufoques. Par example, à un moment donné, on s'est dit que ce qui nous rassemblait le plus était la réalisation d'une **pipe à crack !** En continuant à chercher, on est tombé sur la santé.

![](images/brainstorming.jpg)

### 5.3.1 Identification d'une problématique

Pour approfondir ce brainstorming et nous approcher petit à petit d'une problématique, nous avons retourné la feuille et fait un "_moi à ta place_". Le principe est le suivant : Chacun à son tour, on évoque une problématique (pas forcément en lien avec la santé). On commence toujours nos phrases avec "_moi à ta place, je..._" en rebondissant sur ce que le précédent vient de dire. À nouveau, on ne se retient pas et on dit tout ce qui nous traverse l'esprit. Il arrive un moment où plus personne n'a vraiment d'idée. Ce que nous avons alors fait c'est d'amener quelqu'un d'un autre groupe qui voyait ce qu'on avait déjà écrit, puis lui même proposait une idée, on rebondit dessus et c'est repartit pour un tour. On a fait ça 2-3 fois pour essayer d'élargir un maximum notre pensée. 

L'étape suivante, c'est de restreindre les possibilités. Nous avons alors soulignés les quelques problématiques sur lesquelles nous voudrions bien travailler. Voilà ce que ça donne : 

![](images/Moiplace.jpg)

Un soucis que nous avon eu, c'est qu'on essayait souvent de trouver des solution or ici, il fallait vraiment se focaliser sur des **problèmes**. En effet, si on trouve une solution à un problème qui n'existe pas, ce n'est pas très utile.

## 5.4 Dynamique de groupe

Afin de réaliser notre projet dans les meilleures conditions, le professeur nous a donné quelques outils de gestion de projet, j'en explique quelques uns ici.

### 5.4.1 Organisation d'une réunion

Avant de commencer une réunion, il est bon de savoir de quoi on va parler pour être sur d'avancer. On rédige alors ensemble un ordre du jour avec les différents point que nous allos aborder en prenant bien soin de mettre les point les plus urgents/important au début pour être sûr que le groupe soit bien focus dessus. 

Pour commencer une réunion, il est intéressant de faire ce qu'on appelle une météo d'entrée. Cela consiste à dire comment on se sent avant de commencer la réunion, si on est en colère, fatigué, motiver etc. Cela permet de savoir dans quelles conditions va se dérouler la réunion. Si personne n'est en état, on peut repousser une décision importante à plus tard. On peut aussi dire ce qu'on attend de cette réunion. 

Ensuite, on défini quelques rôles importants pour le bon déroulé de la réunion. Les 2 qui sont les plus utiles à mes yeux sont le secretaire et l'animateur. Le premier s'occupe de prendre un PV de tout ce qu'on dit et de noter les points clés. Le second, quant à lui, s'occupe d'animer la réunion et s'assure que tout le monde ai pris la parole, donné sont avis etc. Quelques méthodes permettent cela, je les développe un peu plus bas. Pour se réparir les rôles, on peut par exemple dire chacun un chiffre entre 0 et 5 sur notre motivation à faire tel ou tel rôle. Le plus motivé s'exécute.

Une fois qu'on a discuté des points que nous voulions aborder ainsi qu'après avoir pris des décision, on fait une "to do list". Elle permet de savoir ce qu'on doit faire pour pouvoir avancer entre 2 réunions. Pour chaque tâche il faut impérativement dire _**Qui fait quoi pour quand?**_. Ainsi, tout le monde sait ce qu'il doit faire et avancer sur sa tâche.

Pour terminer, on fait une météo de sortie pour checker si tout s'est bien passé, si personne n'est frustré de ne pas avoir pu s'exprimer ou si les objectifs de la réunion ont bien été atteint.

### 4.4.2 Prise de décisions

Lors d'un projet, il arrive régulièrement qu'on doive prendre certaines décisions difficiles. Il faut donc trouver un moyen de se mettre d'accord sans créer des tensions. Plusieurs méthodes existent : 

- Le consensus
- L'aléatiore
- Le vote (plein de manière possible)
- etc

Elles peuvent aussi se combiner. Imaginons qu'on vote, et qu'il y a une égalité entre 2 possibilité, on peut lancer une pièce de monnaie por trancher. Dans notre groupe, nous avons décidé ensemble de prendre la méthode du consensus. On essaie de trouver un compromis pour les attentes de tout le monde afin de satisfaire au mieux le groupe. Dans le cas où on ne parvient pas à se mettre d'accord, on fait un vote puis on fait au pif en cas d'égalité. Cette méthode nous convenait tous les 4.

### 4.4.3 Temps de parole

Pour s'assurer que tout le monde s'exprime librement, nous avons vu quelques méthode efficaces. La première c'est le baton de parole. Quand on l'a, on peu parler, sinon on se tait. De cette manière on ne se coupe pas la parole et on peut exprimer correctement le fond de sa pensée. Pour se passer le baton, on peut le faire en tirant au sort en lançant un dé, on peut égalemet le donner à son voisin de gauche.

On peut aussi donner un temps limité à tout le monde et on fait plusieurs tour. Je parle pendant 2 minutes, puis c'est à Morgan, à Léon et enfin à Serge, puis on recommence. Si on a rien à dire, on coupe plus tôt.