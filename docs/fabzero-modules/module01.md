# 1. Gestion de projet et documentation

Au terme de cette première séance, j'ai cloné ma page du site FabZero Experiment sur mon ordinateur. Ainsi, je peux y faire des modification hors ligne et garder une trace de tout nouvel ajout sur Gitlab. Pour se faire, il a fallu créer une connexion sécurisée entre mon ordinateur et Gitlab. Cette page reprend les différentes étapes que j'ai réalisé pour y arriver, les différentes problèmes rencontré, leur solutions ainsi que quelques informations supplémentaires.

## 1.1 Command Line user Interface (CLI)

La Grafical User Interface (**GUI**) ou interface graphique est une partie d'un logiciel permettant à l'utilisateur de discuter avec celui-ci. C'est à dire lui demander d'effectuer certaines commandes en cliquant sur des boutons, ouvrir un menu etc. Pafrois se débarasser d'une telle interface peut être utile. Cela offre plus de libertés et une rapidité améliorée. De plus une GUI n'est pas toujours fournie avec le logiciel.

Les CLI prennent en général la forme d'un terminal dans lequel on va taper des commandes UNIX. Mon ordinateur est équipé du système d'exploitaion Windows qui n'est pas vraiment adapté à l'utilisation d'un terminal contrairement à Linux. Il est cependant possible de contourner ce problème en installant le shell Bash sur Windows (possible depuis Windows 10). Pour ce faire, j'ai suivit la procédure décrite [ici](https://korben.info/installer-shell-bash-linux-windows-10.html). Un premier problèmes est alors survenu lors du lancement. Pour une raison que j'ingore encore, Bash se lançait et s'arrêtait immédiatement.

Une possible solution est d'installer une machine virtuelle Ubuntu sur on ordinateur et ainsi utiliser directement Linux tout en conservant Windows sur mon PC. À chaque fois que je voudrait mettre à jour ma documentation, je devrait allumer ma machine virtuelle ce qui prend un peu de temps (on parle d'une minute max mais cela peut sembler long si on veut juste rajouter une virgule).

Je me suis alors souvenu que j'avais déjà une sorte de Bash sur mon PC que j'avais installé pour faciliter le lien entre mon ordinateur et GitHub. J'ai donc simplement ouvert git Bash déjà présent et tout s'est assez bien déroulé par la suite. 

Voici quelques commandes de bases utiles que j'ai pu trouvé sur [Some Basic UNIX Commands](https://www.tjhsst.edu/~dhyatt/superap/unixcmd.html)

## 1.2 Clé SSH

Pour mettre à jour le site du cours, il faut modifier les fichiers .md (markdown) présent sur le GitLab du cours. On peut également télécharger les fichier sur notre ordinateur, faire nos changements puis les renvoyer sur GitLab. Cela est possible en créant un canal de communication sécurisé entre GitLab et mon ordinateur. Pour ce faire, je commence par mettre à jour la configuration pour que mon username et mon email correspondent à ceux de GitLab de la façon suivante :

![](images/config_git.png)

Maintenant que c'est fait, je crée ma clé ssh à proprement parler. J'ai utilisé le cipher conseillé "ED25519". Les différentes commandes sont reprises ici :

![](images/ssh.png)

Il ne me reste pkus qu'a ajouter cette clé ssh sur gitlab dans l'onglet "SSH Keys". À présent, je peux communiquer facilement et échanger des données entre mon ordinateur et gitlab. Je clone alors le projet Gitlab sur mon PC afin de travailler dessus. Uune fois cela fait, je peux effectuer des modification de ma documentation et les renvoyer sur le site. Pour ce faire, je modifie les fichiers .md sur mon ordinateur et j'effectue les commandes suivante pour les renvoyer :

| Commande|  Description    |
|-----|-----------------------|
| ``` git pull ```  | Télécharger la dernière version du projet
| ``` git add -A ```   | Ajouter toutes les modifications
| ``` git commit -m "message" ```  | Ajouter un message d'explication des changements
| ``` git push ```  | Envoyer nous changements sur le serveur


## 1.3 Documentation

Dans cette section, j'explique les commandes et truc et astuces que j'ai trouvé et qui me permette de modifier ma documentation confortablement.

### 1.3.1 Visual Studio Code

Pour modifier mes .md j'utilise VScode (visual studio code). On peut le télécharcher [ici](https://code.visualstudio.com/download). C'est un environnement assez polyvalent avec une interface facile d'utilisation et qui possèdes des avantages pratiques. Par exemple, je peux directement visualiser ce que fait mon code en temps réel en affichant le code compilé à droite de l'écran. J'utilise le racourci clavier ```ctrl+K V``` ou ```ctrl+shift+V``` pour l'afficher dans un nouvel onglet.

![](images/ctrlkv.png)

### 1.3.2 Commmandes Markdown

| Commande|  Résultat    |
|-----|-----------------------|
| ``` _italique_ ```  | _italique_
| ``` **gras** ```   | **gras**
| ``` **_gras et italique_** ```  | **_gras et italique_**
| ``` ~~barré~~ ```  | ~~barré~~
|` ``` bloc de code ``` `| ``` bloc de code ``` 
|Insérer un lien : ``` [nom du lien](url) ```| [nom du lien](url)
|Insérer une image : ``` ![fleur](images/fleur.jpg)``` | ![fleur](images/fleur.jpg)
|``` # Titre ``` | # Titre
|``` ## Sous titre ``` | ##  Sous titre
|``` ### Sous sous titre ``` | ###  Sous sous titre 

D'autres commandes utiles sont reprise sur [ce site](https://docs.framasoft.org/fr/grav/markdown.html).

Pour changer la taille des images, j'ai utilisé adobe illustrator ou paint 3D.