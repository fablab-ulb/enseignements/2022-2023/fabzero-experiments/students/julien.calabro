# 3. Impression 3D

Dans cette partie, j'explique en quelques mots ce qu'est l'impression 3D, ce qu'elle permet de faire et quelques limitations. J'explique aussi comment j'ai imprimé le flexlink que j'ai créé à la section précédente. Enfin je récupère un code openScad de l'un de mes camarades et je l'adapte au mien afin de créer un méchanisme.

## 3.1 Introduction

L'impression 3D est une technique de fabrication permettant de créer une pièce 3D conçue par ordinateur. Le principe est simple, on construit une pièce sur un logiciel (openscad on freeCAD par example), on l'envoie à l'imprimante et celle ci s'imprime couche par couche un peu comme une imprimante 2D classique qui imprime ligne par ligne sur une feuille. La plupart du temps, "l'encre" est un filament de plastique (PLA entre autre) mais d'autres types d'impression sont possible. L'impression 3D est utilisée dans divers domaines :

- Construction
- Aéronautique
- Agroalimentaire
- Médecine
- ...

L'avantage de cette technique est qu'on peut produire une grande variété pièces très complexe à faible coût. 


Elles sont légères et les possibilités sont presque infinie. Il y a cependant quelques limitations. Toutes les formes ne sont pas possible à imprimer, par example une simple sphère ne peut pas s'imprimer correctement car la surface de contact avec le lit est un point. Cette forme est instable. Toutefois, on peut l'imprimer en ajoutant des bordures afin d'augmenter la surface de contact. Elles seront enlevée par la suite. D'autres pièces comporte des grand espaces vides, une sorte de pont par example. Ce genrre de chose est difficile à imprimer parce que la machine ne peut pas construire dans le vide. Ce problème peut être contourné en ajoutant des supports. La dernière limitation, et pas des moindre, est le temps d'impression. En effet, il n'est pas rare qu'une pièce de quelque cm³ prenne plusieurs heures à s'imprimer. On peut toujours réduire ce problème en diminuant la densité, la qualité et d'autres paramètre mais le temps restera conséquant.

On se rend bien compte que cette technologie ne peut pas vraiment être utilisée à grande échelle.

## 3.2 Impression

Une fois qu'on a conçu notre pièce il est temps de la rendre réele. La première chose à faire est de l'exporter en format .stl. On se retrouve alors avec un fichier 3D. Il faut maintenant le convertir en un code directement interprétable pour l'imprimante. Il s'agit d'un code optimisé décrivant les mouvement que doit réaliser le lit et la tête de l'imprimante. On appel ce format "gcode". Evidemment on ne fait pas ça manuellement, des logiciels existent. Celui que j'utilise est conçu pour les imprimantes du fablab ULB. 

### 3.2.1 PrusaSlicer

Vous l'aurez compris au titre ce cette sous section, ce logiciel s'appelle PursaSlicer, pour le télécharger, c'est par [ici](https://help.prusa3d.com/article/install-prusaslicer_1903). 

J'explique quelques manipulation de base mais plus d'explications sont disponible sur [ce tutoriel](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md).

Une fois le logiciel téléchargé et configuré, il se présente comme ceci :

![PursaSlicer](images/PursaSlicer.png)

Pour ajouter ou enlever une pièce, on utilise les boutons situé en haut à gauche

![Ajout-supression](images/ajout.png)

On peut translater et faire tourner notre objet en utilisant les outils sur la barre de gauche. C'est très utile parce que toute les oriantation ne sont pas possible à imprimer, celle-ci par exemple nécessite d'imprimer au dessus du vide

![vide](images/vide.png)

Sur la droite de l'écran, on peut ajuster quelques règlages : 

![reglages](images/reglages.png)

En haut on gère l'épaisseur des couches, ensuite on dit quel filament on utilise. "Generic PLA" permet d'enter les paramètres d'un PLA quelconque. C'est pratique si on ne sait pas exactement à quel PLA on a affaire ou par facilité. C'est pas le plus optimal mais ça marche. Le 3e paramètre décrit l'imprimante qu'on veut utiliser. Sur cette image, ce sont les imprimantes di fablab. On peut également décider de mettre des support ou non, ici ce n'est pas nécessaire, pareil pour la bordure. Enfin, on peut régler la densité du remplissage ainsi que son pattern. Plus de paramètres son disponible en cliquant sur les engrenages de droite.

Une fois que tout les paramètres sont bons, on peut faire "découper maintenant" pour voir notre pièce tranche par tranche.

![slice](images/slices.png)

On peut voir égalemet le temps d'impression pour chaque partie et le temps total en bas à droite.

![temps](images/temps.png)

### 3.2.2 Impression du flexlink

On peut alors exporter le Gcode, le mettre sur une carte SD pour lancer l'impression.

Avant d'imprimer, il faut nettoyer soigneusement la tête et le plateau afin que le PLA accroche bien.

J'ai imprimé une pièce plus petite que celle sur les images pour réduire le temps d'impression. Je me retrouve donc avec mon flexlink qui à mis 40 minutes à s'imprimer.

PHOTOOOOOOS

## 3.3 Assemblage

J'ai récupéré le code de Karl Preux pour faire cette partie. Il a réalisé un bloc LEGO que voici :

![lego](images/LEGO.jpg)

J'ai fait quelques modifications et je me retrouve avec ces 2 pièces supplémentaires que je vais pouvoir ajouter à la mienne.

![lego1](images/lego1.png)
![lego2](images/lego2.png)

Voici les pièces imprimées : 

![](images/lego12.jpg)
![](images/flexlinkph.jpg)
![](images/desasemble.jpg)
![](images/tout.jpg)


Une fois assemblé, la catapulte est prête !

VIDEOOOO

## 3.4 Problèmes rencontrés

Ces impressions ne se sont pas toujours passée comme prévu. En effet, j'ai eu quelques problèmes.

### 3.4.1 Manque d'adhérence

Lorsque j'ai voulu imprimer les pièces LEGO, il est arrivé que la pièce se décroche du plateau alors que je l'avais soigneusement nettoyé. La surface de contact avec le sol et assez faible en réalité. Pour résoudre cela, j'ai ajouter des bordures sur PursaSlicer. Les composant avaient alors une meilleure adhérence et ne se décrochaient plus.

### 3.4.1 Oubli de supports

Quand j'ai imprimé mon flexlink, j'ai oublié de rajouter un support pour soutenir la partie flexible, je m'en suis rendu compte en cours d'impression et je l'ai stoppé. Voilà ce que ça donne:

![](images/nosupport.jpg)

J'ai relancé en ajoutant ces supports et tout s'est bien passé.

### 3.4.3 Problème de fimament
 Sur une des impressions, l'imprimante s'est arretée pour me demander de décharger le filament et ce plusieurs fois pendant l'impression. J'ai pas trop compris pourquoi, j'ai relancé l'impression a chaque fois et ça a fini par fonctionner. Je pense que c'est un problème avec la machine.

![](images/filament.jpg)