// File : lego1.scad

// Author : Julien Calabro

//Date : 7 mars 2023

// License : [Creative Commons Attribution-ShareAlike 4.0 International CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

// Crédits : Le travail original à été réalisé par Karl Preux sous la license CC BY-SA

 // ici il suifit d'introduire le nombre de colonne et de ligne voulue pour avoir un bon lego {

colonne_lego = 3;
ligne_lego = 2;

// parameter: dimension modifié et adapté à un vrai légo 

hauteur_c1 = 9.60;
hauteur_cil1 = 10.0; //hauteur du cilindre 1
rayon_cil = 1.85; // rayon des cylindre
rayon_cil2 = 3.25; //rayon cylindre intérieurdu cube 
colone= colonne_lego -1 ;//Nombre de colone de cylindre toujours +1
ligne = ligne_lego -1;//nombre de ligne de cylindre toujours +1
longueur_c1 = (ligne+1)*5.95;//31.80; //c1 correspond au cube 1
largeur_c1 = (colone+1)*5.9;
$fn =25;


difference(){
    cube([longueur_c1, largeur_c1, hauteur_c1]);
    translate([1.45,1.45])//centre le trou 
    cube([longueur_c1-2.90, largeur_c1-2.90, hauteur_c1-1]);
}

translate([3.90, 3.90])
for (j=[0:colone]){
    for (i = [0:ligne]){
        translate([i*5,j*5,hauteur_c1])
        cylinder(h=hauteur_cil1,r=rayon_cil);
        }
    }


